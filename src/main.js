// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router.js'
import store from './store.js'
import Vant from 'vant';

import 'vant/lib/index.css';
Vue.use(Vant);

// 加载全局样式（最好放到最后，方便去覆盖第三方样式）
import './assets/css/index.less'


Vue.config.productionTip = false

// 导入 axios 并挂载
import request from './utils/request.js'
import './utils/dayjs'

Vue.prototype.$http = request

// 创建全局的时间过滤器    data   date
// import moment from 'moment' // 导入格式化时间的包
Vue.filter('dateFormat', function (val) {
  // 借助于 moment.js 包来快速格式化时间
  // return moment(val).format('YYYY-MM-DD HH:mm:ss')
  const dt = new Date(val)

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1).toString().padStart(2, '0')
  const d = dt.getDate().toString().padStart(2, '0')

  const hh = dt.getHours().toString().padStart(2, '0')
  const mm = dt.getMinutes().toString().padStart(2, '0')
  const ss = dt.getSeconds().toString().padStart(2, '0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router, store,
  components: { App },
  template: '<App/>'
})
