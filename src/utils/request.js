import axios from 'axios'
//import { Indicator } from "mint-ui";
const qs = require('qs')

if (process.env.NODE_ENV === 'development'){
  //axios.defaults.baseURL = '/api' 
  axios.defaults.baseURL = "http://218.77.73.70:8668/__myapp/myAdminPhp/index.php?s="
}else{
  //axios.defaults.baseURL =  'index.php?s=' //baseUrl
  axios.defaults.baseURL = "http://www.zpxwt.top/xwt/index.php?s="
}

//axios.defaults.baseURL = "http://218.77.73.70:8668/__myapp/myAdminPhp/index.php?s="

//axios.defaults.withCredentials = true

/* axios.defaults.transformRequest =  function (data) {
  data = qs.stringify(data) //序列化参数 
  return data 
} */


 

// request拦截器
axios.interceptors.request.use(config => {
  //if (store.getters.token) {
   //config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  //}
  //thinkphp 改造

  //Indicator.open({ text: "加载中...", spinnerType: "fading-circle" });
  
  if (config.method=='get' && config.params){
    config.url += '&' + qs.stringify(config.params)
    config.params= null;
  }
  config.data = qs.stringify(config.data)


  return config
}, error => {
  console.log(error, "axios请求错误")
  Promise.reject(error)
})

// respone拦截器
axios.interceptors.response.use(
  response => {
    //Indicator.close();
    //登录错误
    if (response.data.status && response.data.status==-1){
      /* Message({
        message: "请先注销后重新登录",
        type: 'error',
        duration: 5 * 1000
      }) */
      return Promise.reject(error)
    }

    return response.data

  },
  error => {
    //console.log('err' + error)// for debug
    /* Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    }) */
    return Promise.reject(error)
  }
)

export default axios
